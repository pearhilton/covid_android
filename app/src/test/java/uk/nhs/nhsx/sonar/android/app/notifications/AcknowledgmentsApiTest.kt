/*
 * Copyright © 2020 Liberty Apps. All rights reserved.
 */

package uk.co.libertyapps.covid.app.notifications

import com.android.volley.Request.Method.PUT
import org.assertj.core.api.Assertions.assertThat
import org.junit.Test
import uk.co.libertyapps.covid.app.http.HttpClient
import uk.co.libertyapps.covid.app.http.TestQueue

class AcknowledgmentsApiTest {

    private val requestQueue = TestQueue()
    private val httpClient = HttpClient(requestQueue, "someValue")
    private val acknowledgmentsApi = AcknowledgmentsApi(httpClient)

    @Test
    fun `test send()`() {
        acknowledgmentsApi.send("https://api.example.com/ack/10012")

        val request = requestQueue.lastRequest
        assertThat(request.method).isEqualTo(PUT)
        assertThat(request.url).isEqualTo("https://api.example.com/ack/10012")
    }
}
