/*
 * Copyright © 2020 Liberty Apps. All rights reserved.
 */

package uk.co.libertyapps.covid.app

import android.content.Intent
import io.mockk.Called
import io.mockk.every
import io.mockk.mockk
import io.mockk.verifyAll
import org.junit.Test
import testsupport.TestIntent
import testsupport.mockContextWithMockedAppComponent
import uk.co.libertyapps.covid.app.notifications.Reminders
import uk.co.libertyapps.covid.app.status.UserState
import uk.co.libertyapps.covid.app.status.UserStateStorage

class PackageReplacedReceiverTest {

    private val stateStorage = mockk<UserStateStorage>()
    private val reminders = mockk<Reminders>()
    private val context = mockContextWithMockedAppComponent()

    private val receiver = PackageReplacedReceiver().also {
        it.userStateStorage = stateStorage
        it.reminders = reminders
    }

    @Test
    fun `onReceive - with unknown intent action`() {
        val intent = TestIntent("SOME_OTHER_ACTION")

        receiver.onReceive(context, intent)

        verifyAll {
            stateStorage wasNot Called
        }
    }

    @Test
    fun `onReceive - with package-replaced intent action`() {
        val userState = mockk<UserState>()
        every { userState.scheduleCheckInReminder(reminders) } returns Unit
        every { stateStorage.get() } returns userState

        receiver.onReceive(context, TestIntent(Intent.ACTION_MY_PACKAGE_REPLACED))

        verifyAll {
            userState.scheduleCheckInReminder(reminders)
        }
    }
}
