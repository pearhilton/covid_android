/*
 * Copyright © 2020 Liberty Apps. All rights reserved.
 */

package uk.co.libertyapps.covid.app.http

import javax.crypto.KeyGenerator
import javax.crypto.SecretKey

fun generateSignatureKey(): SecretKey =
    KeyGenerator.getInstance("HMACSHA256").generateKey()
