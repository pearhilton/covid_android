/*
 * Copyright © 2020 Liberty Apps. All rights reserved.
 */

package testsupport

import android.content.Context
import io.mockk.every
import io.mockk.mockk
import uk.co.libertyapps.covid.app.appComponent
import uk.co.libertyapps.covid.app.di.ApplicationComponent

fun mockContextWithMockedAppComponent(): Context {
    val context = mockk<Context>()
    val appComponent = mockk<ApplicationComponent>(relaxUnitFun = true)
    every { context.appComponent } returns appComponent
    return context
}
