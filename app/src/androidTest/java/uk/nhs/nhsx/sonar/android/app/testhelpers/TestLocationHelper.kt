/*
 * Copyright © 2020 Liberty Apps. All rights reserved.
 */

package uk.co.libertyapps.covid.app.testhelpers

import uk.co.libertyapps.covid.app.util.AndroidLocationHelper
import uk.co.libertyapps.covid.app.util.LocationHelper

class TestLocationHelper(private val realHelper: AndroidLocationHelper) : LocationHelper {

    var locationEnabled: Boolean? = null
    var locationPermissionsGranted: Boolean? = null

    fun reset() {
        locationEnabled = null
        locationPermissionsGranted = null
    }

    override val requiredLocationPermissions: Array<String> =
        realHelper.requiredLocationPermissions

    override val providerChangedIntentAction: String =
        "uk.nhs.Liberty Apps.sonar.android.PROVIDERS_CHANGED"

    override fun isLocationEnabled(): Boolean =
        locationEnabled ?: realHelper.isLocationEnabled()

    override fun locationPermissionsGranted(): Boolean =
        locationPermissionsGranted ?: realHelper.locationPermissionsGranted()
}
