/*
 * Copyright © 2020 Liberty Apps. All rights reserved.
 */

package uk.co.libertyapps.covid.app.testhelpers

import android.bluetooth.BluetoothAdapter
import android.content.Context
import com.polidea.rxandroidble2.RxBleClient
import dagger.Component
import dagger.Module
import dagger.Provides
import org.joda.time.DateTime
import uk.co.libertyapps.covid.app.AppDatabase
import uk.co.libertyapps.covid.app.DeviceDetection
import uk.co.libertyapps.covid.app.analytics.AnalyticEvent
import uk.co.libertyapps.covid.app.analytics.SonarAnalytics
import uk.co.libertyapps.covid.app.ble.BleEvents
import uk.co.libertyapps.covid.app.ble.SaveContactWorker
import uk.co.libertyapps.covid.app.ble.Scanner
import uk.co.libertyapps.covid.app.di.ApplicationComponent
import uk.co.libertyapps.covid.app.di.module.AppModule
import uk.co.libertyapps.covid.app.di.module.BluetoothModule
import uk.co.libertyapps.covid.app.di.module.CryptoModule
import uk.co.libertyapps.covid.app.di.module.NetworkModule
import uk.co.libertyapps.covid.app.di.module.PersistenceModule
import uk.co.libertyapps.covid.app.http.KeyStorage
import uk.co.libertyapps.covid.app.onboarding.OnboardingStatusProvider
import uk.co.libertyapps.covid.app.referencecode.ReferenceCodeProvider
import uk.co.libertyapps.covid.app.registration.ActivationCodeProvider
import uk.co.libertyapps.covid.app.registration.SonarIdProvider
import uk.co.libertyapps.covid.app.registration.TokenRetriever
import uk.co.libertyapps.covid.app.status.UserStateStorage
import javax.inject.Singleton

@Singleton
@Component(
    modules = [
        AppModule::class,
        PersistenceModule::class,
        BluetoothModule::class,
        CryptoModule::class,
        NetworkModule::class,
        TestNotificationsModule::class
    ]
)
interface TestAppComponent : ApplicationComponent {
    fun getSonarIdProvider(): SonarIdProvider
    fun getKeyStorage(): KeyStorage
    fun getAppDatabase(): AppDatabase
    fun getUserStateStorage(): UserStateStorage
    fun getOnboardingStatusProvider(): OnboardingStatusProvider
    fun getActivationCodeProvider(): ActivationCodeProvider
    fun getReferenceCodeProvider(): ReferenceCodeProvider
}

class TestBluetoothModule(
    private val appContext: Context,
    private val rxBleClient: RxBleClient,
    private val currentTimestampProvider: () -> DateTime,
    private val scanIntervalLength: Int = 2
) : BluetoothModule(appContext, scanIntervalLength) {

    override fun provideRxBleClient(): RxBleClient =
        rxBleClient

    override fun provideScanner(
        rxBleClient: RxBleClient,
        saveContactWorker: SaveContactWorker,
        bleEvents: BleEvents
    ) =
        Scanner(
            rxBleClient,
            saveContactWorker,
            bleEvents,
            currentTimestampProvider,
            scanIntervalLength
        )

    var simulateUnsupportedDevice = false

    var simulateTablet = false

    override fun provideDeviceDetection(): DeviceDetection =
        if (simulateUnsupportedDevice || simulateTablet) {
            val adapter = if (simulateUnsupportedDevice) null else BluetoothAdapter.getDefaultAdapter()
            DeviceDetection(adapter, appContext, simulateTablet)
        } else {
            super.provideDeviceDetection()
        }

    fun reset() {
        simulateUnsupportedDevice = false
        simulateTablet = false
    }
}

@Module
class TestNotificationsModule {

    @Provides
    fun provideTokenRetriever(): TokenRetriever =
        TestTokenRetriever()
}

class TestAnalytics : SonarAnalytics {
    override fun trackEvent(event: AnalyticEvent) {
        // Do nothing
    }
}
