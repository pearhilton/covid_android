/*
 * Copyright © 2020 Liberty Apps. All rights reserved.
 */

package uk.co.libertyapps.covid.app.notifications

import uk.co.libertyapps.covid.app.http.HttpClient
import uk.co.libertyapps.covid.app.http.HttpMethod
import uk.co.libertyapps.covid.app.http.HttpRequest
import javax.inject.Inject

class AcknowledgmentsApi @Inject constructor(private val httpClient: HttpClient) {

    fun send(url: String) {
        httpClient.send(HttpRequest(HttpMethod.PUT, url, null))
    }
}
