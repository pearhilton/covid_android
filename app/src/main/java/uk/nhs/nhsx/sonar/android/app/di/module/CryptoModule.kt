/*
 * Copyright © 2020 Liberty Apps. All rights reserved.
 */

package uk.co.libertyapps.covid.app.di.module

import android.content.Context
import dagger.Module
import dagger.Provides
import uk.co.libertyapps.covid.app.http.AndroidSecretKeyStorage
import uk.co.libertyapps.covid.app.http.DelegatingKeyStore
import uk.co.libertyapps.covid.app.crypto.BluetoothIdProvider
import uk.co.libertyapps.covid.app.crypto.BluetoothIdSigner
import uk.co.libertyapps.covid.app.crypto.Encrypter
import uk.co.libertyapps.covid.app.http.KeyStorage
import uk.co.libertyapps.covid.app.http.PublicKeyStorage
import uk.co.libertyapps.covid.app.http.SecretKeyStorage
import uk.co.libertyapps.covid.app.http.SharedPreferencesPublicKeyStorage
import java.security.KeyStore
import uk.co.libertyapps.covid.app.registration.SonarIdProvider

@Module
class CryptoModule(
    private val context: Context,
    private val keyStore: KeyStore
) {
    @Provides
    fun provideEncryptionKeyStorage(
        secretKeyStorage: SecretKeyStorage,
        publicKeyStorage: PublicKeyStorage
    ): KeyStorage = DelegatingKeyStore(secretKeyStorage, publicKeyStorage)

    @Provides
    fun provideSecretKeyStorage(): SecretKeyStorage = AndroidSecretKeyStorage(keyStore, context)

    @Provides
    fun providePublicKeyStorage(): PublicKeyStorage = SharedPreferencesPublicKeyStorage(context)

    @Provides
    fun provideBluetoothCryptogramProvider(
        sonarIdProvider: SonarIdProvider,
        encrypter: Encrypter,
        signer: BluetoothIdSigner
    ): BluetoothIdProvider =
        BluetoothIdProvider(sonarIdProvider, encrypter, signer)
}
