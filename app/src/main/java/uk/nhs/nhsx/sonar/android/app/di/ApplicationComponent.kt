/*
 * Copyright © 2020 Liberty Apps. All rights reserved.
 */

package uk.co.libertyapps.covid.app.di

import com.polidea.rxandroidble2.RxBleClient
import dagger.Component
import uk.co.libertyapps.covid.app.BaseActivity
import uk.co.libertyapps.covid.app.BootCompletedReceiver
import uk.co.libertyapps.covid.app.FlowTestStartActivity
import uk.co.libertyapps.covid.app.MainActivity
import uk.co.libertyapps.covid.app.PackageReplacedReceiver
import uk.co.libertyapps.covid.app.ble.BluetoothService
import uk.co.libertyapps.covid.app.contactevents.DeleteOutdatedEventsWorker
import uk.co.libertyapps.covid.app.debug.TesterActivity
import uk.co.libertyapps.covid.app.di.module.AppModule
import uk.co.libertyapps.covid.app.di.module.BluetoothModule
import uk.co.libertyapps.covid.app.di.module.CryptoModule
import uk.co.libertyapps.covid.app.di.module.NetworkModule
import uk.co.libertyapps.covid.app.di.module.NotificationsModule
import uk.co.libertyapps.covid.app.di.module.PersistenceModule
import uk.co.libertyapps.covid.app.diagnose.DiagnoseCloseActivity
import uk.co.libertyapps.covid.app.diagnose.DiagnoseCoughActivity
import uk.co.libertyapps.covid.app.diagnose.DiagnoseSubmitActivity
import uk.co.libertyapps.covid.app.diagnose.DiagnoseTemperatureActivity
import uk.co.libertyapps.covid.app.diagnose.SubmitContactEventsWorker
import uk.co.libertyapps.covid.app.diagnose.review.DiagnoseReviewActivity
import uk.co.libertyapps.covid.app.notifications.NotificationService
import uk.co.libertyapps.covid.app.notifications.ReminderBroadcastReceiver
import uk.co.libertyapps.covid.app.onboarding.EnableLocationActivity
import uk.co.libertyapps.covid.app.onboarding.GrantLocationPermissionActivity
import uk.co.libertyapps.covid.app.onboarding.PermissionActivity
import uk.co.libertyapps.covid.app.onboarding.PostCodeActivity
import uk.co.libertyapps.covid.app.referencecode.ReferenceCodeWorker
import uk.co.libertyapps.covid.app.registration.RegistrationWorker
import uk.co.libertyapps.covid.app.status.AtRiskActivity
import uk.co.libertyapps.covid.app.status.IsolateActivity
import uk.co.libertyapps.covid.app.status.OkActivity
import uk.co.libertyapps.covid.app.util.LocationHelper
import javax.inject.Singleton

@Singleton
@Component(
    modules = [
        AppModule::class,
        PersistenceModule::class,
        BluetoothModule::class,
        CryptoModule::class,
        NetworkModule::class,
        NotificationsModule::class
    ]
)
interface ApplicationComponent {
    fun inject(activity: BaseActivity)
    fun inject(activity: PermissionActivity)
    fun inject(activity: EnableLocationActivity)
    fun inject(activity: GrantLocationPermissionActivity)
    fun inject(activity: IsolateActivity)
    fun inject(activity: OkActivity)
    fun inject(activity: AtRiskActivity)
    fun inject(activity: DiagnoseReviewActivity)
    fun inject(activity: DiagnoseCloseActivity)
    fun inject(activity: MainActivity)
    fun inject(activity: FlowTestStartActivity)
    fun inject(activity: PostCodeActivity)
    fun inject(activity: TesterActivity)
    fun inject(activity: DiagnoseSubmitActivity)
    fun inject(activity: DiagnoseCoughActivity)
    fun inject(activity: DiagnoseTemperatureActivity)

    fun inject(service: BluetoothService)
    fun inject(service: NotificationService)

    fun inject(worker: DeleteOutdatedEventsWorker)
    fun inject(worker: RegistrationWorker)
    fun inject(worker: SubmitContactEventsWorker)
    fun inject(worker: ReferenceCodeWorker)

    fun inject(receiver: PackageReplacedReceiver)
    fun inject(receiver: BootCompletedReceiver)
    fun inject(receiver: ReminderBroadcastReceiver)

    fun provideRxBleClient(): RxBleClient
    fun provideLocationHelper(): LocationHelper
}
