/*
 * Copyright © 2020 Liberty Apps. All rights reserved.
 */

package uk.co.libertyapps.covid.app.di.module

import dagger.Module
import dagger.Provides
import uk.co.libertyapps.covid.app.registration.FirebaseTokenRetriever
import uk.co.libertyapps.covid.app.registration.TokenRetriever

@Module
class NotificationsModule {

    @Provides
    fun provideTokenRetriever(implementation: FirebaseTokenRetriever): TokenRetriever =
        implementation
}
