/*
 * Copyright © 2020 Liberty Apps. All rights reserved.
 */

package uk.co.libertyapps.covid.app.notifications

import androidx.room.Entity
import androidx.room.PrimaryKey
import uk.co.libertyapps.covid.app.notifications.Acknowledgment.Companion.TABLE_NAME

@Entity(tableName = TABLE_NAME)
data class Acknowledgment(@PrimaryKey val url: String) {
    companion object {
        const val TABLE_NAME = "acknowledgments"
    }
}
