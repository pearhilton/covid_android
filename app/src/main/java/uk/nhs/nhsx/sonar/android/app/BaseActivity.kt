/*
 * Copyright © 2020 Liberty Apps. All rights reserved.
 */

package uk.co.libertyapps.covid.app

import android.bluetooth.BluetoothAdapter.STATE_OFF
import android.os.Bundle
import io.reactivex.disposables.Disposable
import uk.co.libertyapps.covid.app.ble.BluetoothStateBroadcastReceiver
import uk.co.libertyapps.covid.app.ble.LocationProviderChangedReceiver
import uk.co.libertyapps.covid.app.edgecases.ReAllowGrantLocationPermissionActivity
import uk.co.libertyapps.covid.app.edgecases.ReEnableBluetoothActivity
import uk.co.libertyapps.covid.app.edgecases.ReEnableLocationActivity
import uk.co.libertyapps.covid.app.util.LocationHelper
import uk.co.libertyapps.covid.app.util.isBluetoothDisabled
import javax.inject.Inject

abstract class BaseActivity : ColorInversionAwareActivity() {

    private var locationSubscription: Disposable? = null

    @Inject
    lateinit var locationHelper: LocationHelper

    private lateinit var locationProviderChangedReceiver: LocationProviderChangedReceiver

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        appComponent.inject(this)

        locationProviderChangedReceiver = LocationProviderChangedReceiver(locationHelper)
    }

    override fun onResume() {
        super.onResume()
        listenBluetoothChange()
        checkLocationPermission()
        listenLocationChange()
    }

    private fun listenBluetoothChange() {
        if (isBluetoothDisabled()) {
            ReEnableBluetoothActivity.start(this)
        }
        bluetoothStateBroadcastReceiver.register(this)
    }

    private fun checkLocationPermission() {
        if (!locationHelper.locationPermissionsGranted()) {
            ReAllowGrantLocationPermissionActivity.start(this)
        }
    }

    private fun listenLocationChange() {
        locationProviderChangedReceiver.register(this)

        locationSubscription =
            locationProviderChangedReceiver.getLocationStatus().subscribe { isLocationEnabled ->
                if (!isLocationEnabled) {
                    ReEnableLocationActivity.start(this)
                }
            }
    }

    override fun onPause() {
        super.onPause()
        unregisterReceiver(bluetoothStateBroadcastReceiver)
        unregisterReceiver(locationProviderChangedReceiver)
        locationSubscription?.dispose()
    }

    private val bluetoothStateBroadcastReceiver =
        BluetoothStateBroadcastReceiver { state ->
            if (state == STATE_OFF) {
                ReEnableBluetoothActivity.start(this)
            }
        }
}
