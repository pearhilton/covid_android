/*
 * Copyright © 2020 Liberty Apps. All rights reserved.
 */

package uk.co.libertyapps.covid.app.diagnose.review.spinner

import android.widget.Spinner
import androidx.appcompat.content.res.AppCompatResources.getDrawable

fun Spinner.setError() {
    background =
        getDrawable(context, uk.co.libertyapps.covid.app.R.drawable.spinner_background_error)
}

fun Spinner.setInitial() {
    background =
        getDrawable(context, uk.co.libertyapps.covid.app.R.drawable.spinner_background_normal)
}

fun Spinner.setFocused() {
    background =
        getDrawable(context, uk.co.libertyapps.covid.app.R.drawable.spinner_background_focused)
}
