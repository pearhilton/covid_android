/*
 * Copyright © 2020 Liberty Apps. All rights reserved.
 */

package uk.co.libertyapps.covid.app.util

import android.bluetooth.BluetoothAdapter

fun isBluetoothEnabled() = BluetoothAdapter.getDefaultAdapter().isEnabled
fun isBluetoothDisabled() = !isBluetoothEnabled()
