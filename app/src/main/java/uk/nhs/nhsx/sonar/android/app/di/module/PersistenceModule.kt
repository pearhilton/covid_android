/*
 * Copyright © 2020 Liberty Apps. All rights reserved.
 */

package uk.co.libertyapps.covid.app.di.module

import android.content.Context
import androidx.room.Room
import dagger.Module
import dagger.Provides
import uk.co.libertyapps.covid.app.AppDatabase
import uk.co.libertyapps.covid.app.contactevents.CoLocationDataProvider
import uk.co.libertyapps.covid.app.contactevents.ContactEventDao
import uk.co.libertyapps.covid.app.notifications.AcknowledgmentsDao

@Module
class PersistenceModule(private val appContext: Context) {

    @Provides
    fun provideDatabase() =
        Room
            .databaseBuilder(appContext, AppDatabase::class.java, "event-database")
            .fallbackToDestructiveMigration()
            .build()

    @Provides
    fun provideContactEventDao(database: AppDatabase): ContactEventDao =
        database.contactEventDao()

    @Provides
    fun provideCoLocationDataProvider(dao: ContactEventDao): CoLocationDataProvider =
        CoLocationDataProvider(dao)

    @Provides
    fun provideAcknowledgmentsDao(database: AppDatabase): AcknowledgmentsDao =
        database.acknowledgmentsDao()
}
