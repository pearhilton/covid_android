/*
 * Copyright © 2020 Liberty Apps. All rights reserved.
 */

package uk.co.libertyapps.covid.app.debug

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.activity_test.continue_button
import kotlinx.android.synthetic.main.activity_test.events
import kotlinx.android.synthetic.main.activity_test.exportButton
import kotlinx.android.synthetic.main.activity_test.no_events
import kotlinx.android.synthetic.main.activity_test.reset_button
import kotlinx.android.synthetic.main.activity_test.sonar_id
import timber.log.Timber
import uk.co.libertyapps.covid.app.R
import uk.co.libertyapps.covid.app.ViewModelFactory
import uk.co.libertyapps.covid.app.appComponent
import uk.co.libertyapps.covid.app.onboarding.OnboardingStatusProvider
import uk.co.libertyapps.covid.app.registration.ActivationCodeProvider
import uk.co.libertyapps.covid.app.registration.SonarIdProvider
import uk.co.libertyapps.covid.app.status.UserStateStorage
import javax.inject.Inject

class TesterActivity : AppCompatActivity(R.layout.activity_test) {

    @Inject
    lateinit var userStateStorage: UserStateStorage

    @Inject
    lateinit var sonarIdProvider: SonarIdProvider

    @Inject
    lateinit var activationCodeProvider: ActivationCodeProvider

    @Inject
    lateinit var onboardingStatusProvider: OnboardingStatusProvider

    @Inject
    lateinit var viewModelFactory: ViewModelFactory<TestViewModel>

    private val viewModel: TestViewModel by viewModels { viewModelFactory }

    override fun onCreate(savedInstanceState: Bundle?) {
        appComponent.inject(this)
        super.onCreate(savedInstanceState)
        sonar_id.text = "${sonarIdProvider.getSonarId()}"
        val adapter = EventsAdapter()
        events.adapter = adapter
        events.layoutManager = LinearLayoutManager(this)

        continue_button.setOnClickListener {
            finish()
        }

        reset_button.setOnClickListener {
            userStateStorage.clear()
            sonarIdProvider.clear()
            onboardingStatusProvider.clear()
            activationCodeProvider.clear()
            viewModel.clear()
        }

        exportButton.setOnClickListener {
            viewModel.storeEvents(this)
        }

        viewModel.observeConnectionEvents().observe(this, Observer {
            Timber.d("Devices are $it")
            if (it.isEmpty()) no_events.visibility = View.VISIBLE
            else {
                no_events.visibility = View.GONE
                val ids = it.map { event -> event.id }.distinct()
                val unique = ids.map { id -> it.findLast { event -> event.id == id } }
                adapter.submitList(unique)
            }
        })

        viewModel.observeConnectionEvents()
    }

    companion object {
        fun start(context: Context) =
            context.startActivity(getIntent(context))

        private fun getIntent(context: Context) =
            Intent(context, TesterActivity::class.java)
    }
}
