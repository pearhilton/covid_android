/*
 * Copyright © 2020 Liberty Apps. All rights reserved.
 */

package uk.co.libertyapps.covid.app.di.module

import dagger.Module
import dagger.Provides
import uk.co.libertyapps.covid.app.diagnose.review.CoLocationApi
import uk.co.libertyapps.covid.app.http.HttpClient
import uk.co.libertyapps.covid.app.http.KeyStorage
import uk.co.libertyapps.covid.app.referencecode.ReferenceCodeApi
import uk.co.libertyapps.covid.app.registration.ResidentApi
import uk.co.libertyapps.covid.app.registration.SonarIdProvider

@Module
class NetworkModule(
    private val baseUrl: String,
    private val sonarHeaderValue: String
) {

    @Provides
    fun provideHttpClient(): HttpClient =
        HttpClient(sonarHeaderValue)

    @Provides
    fun residentApi(keyStorage: KeyStorage, httpClient: HttpClient): ResidentApi =
        ResidentApi(baseUrl, keyStorage, httpClient)

    @Provides
    fun coLocationApi(keyStorage: KeyStorage, httpClient: HttpClient): CoLocationApi =
        CoLocationApi(baseUrl, keyStorage, httpClient)

    @Provides
    fun referenceCodeApi(
        sonarIdProvider: SonarIdProvider,
        keyStorage: KeyStorage,
        httpClient: HttpClient
    ): ReferenceCodeApi =
        ReferenceCodeApi(baseUrl, sonarIdProvider, keyStorage, httpClient)
}
