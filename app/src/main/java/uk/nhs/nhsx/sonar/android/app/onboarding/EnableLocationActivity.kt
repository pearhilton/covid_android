/*
 * Copyright © 2020 Liberty Apps. All rights reserved.
 */

package uk.co.libertyapps.covid.app.onboarding

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS
import io.reactivex.disposables.Disposable
import kotlinx.android.synthetic.main.activity_edge_case.edgeCaseText
import kotlinx.android.synthetic.main.activity_edge_case.edgeCaseTitle
import kotlinx.android.synthetic.main.activity_edge_case.takeActionButton
import kotlinx.android.synthetic.main.banner.toolbar_info
import uk.co.libertyapps.covid.app.ColorInversionAwareActivity
import uk.co.libertyapps.covid.app.R
import uk.co.libertyapps.covid.app.appComponent
import uk.co.libertyapps.covid.app.ble.LocationProviderChangedReceiver
import uk.co.libertyapps.covid.app.util.LocationHelper
import uk.co.libertyapps.covid.app.util.URL_INFO
import uk.co.libertyapps.covid.app.util.openUrl
import javax.inject.Inject

open class EnableLocationActivity : ColorInversionAwareActivity(R.layout.activity_edge_case) {

    @Inject
    lateinit var locationHelper: LocationHelper

    private var locationSubscription: Disposable? = null
    private lateinit var locationProviderChangedReceiver: LocationProviderChangedReceiver

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        appComponent.inject(this)

        edgeCaseTitle.setText(R.string.enable_location_service_title)
        edgeCaseText.setText(R.string.enable_location_service_rationale)
        takeActionButton.setText(R.string.go_to_your_settings)

        toolbar_info.setOnClickListener {
            openUrl(URL_INFO)
        }

        takeActionButton.setOnClickListener {
            startActivity(Intent(ACTION_LOCATION_SOURCE_SETTINGS))
        }

        locationProviderChangedReceiver = LocationProviderChangedReceiver(locationHelper)
    }

    override fun onResume() {
        super.onResume()
        if (locationHelper.isLocationEnabled()) {
            finish()
        }
        locationProviderChangedReceiver.register(this)

        locationSubscription =
            locationProviderChangedReceiver.getLocationStatus().subscribe { isLocationEnabled ->
                if (isLocationEnabled) {
                    finish()
                }
            }
    }

    override fun handleInversion(inversionModeEnabled: Boolean) {
        if (inversionModeEnabled) {
            takeActionButton.setBackgroundResource(R.drawable.button_round_background_inversed)
        } else {
            takeActionButton.setBackgroundResource(R.drawable.button_round_background)
        }
    }

    override fun onPause() {
        super.onPause()
        unregisterReceiver(locationProviderChangedReceiver)
        locationSubscription?.dispose()
    }

    companion object {
        fun start(context: Context) =
            context.startActivity(getIntent(context))

        private fun getIntent(context: Context) =
            Intent(context, EnableLocationActivity::class.java)
    }
}
