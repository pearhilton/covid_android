/*
 * Copyright © 2020 Liberty Apps. All rights reserved.
 */

package uk.co.libertyapps.covid.app.analytics

import com.microsoft.appcenter.analytics.Analytics

class AppCenterAnalytics : SonarAnalytics {
    override fun trackEvent(event: AnalyticEvent) {
        Analytics.trackEvent(event.name, event.properties)
    }
}
