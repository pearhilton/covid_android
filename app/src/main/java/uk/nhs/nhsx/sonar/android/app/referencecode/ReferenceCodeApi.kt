/*
 * Copyright © 2020 Liberty Apps. All rights reserved.
 */

package uk.co.libertyapps.covid.app.referencecode

import uk.co.libertyapps.covid.app.http.HttpClient
import uk.co.libertyapps.covid.app.http.HttpMethod.PUT
import uk.co.libertyapps.covid.app.http.HttpRequest
import uk.co.libertyapps.covid.app.http.Promise
import uk.co.libertyapps.covid.app.http.SecretKeyStorage
import uk.co.libertyapps.covid.app.http.jsonObjectOf
import uk.co.libertyapps.covid.app.registration.SonarIdProvider

class ReferenceCodeApi(
    private val baseUrl: String,
    private val sonarIdProvider: SonarIdProvider,
    private val secretKeyStorage: SecretKeyStorage,
    private val httpClient: HttpClient
) {

    fun generate(): Promise<ReferenceCode> {
        val secretKey = secretKeyStorage.provideSecretKey()
        val sonarId = sonarIdProvider.getSonarId()
        val url = "$baseUrl/api/residents/$sonarId/linking-id"

        return httpClient
            .send(HttpRequest(PUT, url, jsonObjectOf(), secretKey))
            .map { ReferenceCode(it.getString("linkingId")) }
    }
}
