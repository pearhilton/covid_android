/*
 * Copyright © 2020 Liberty Apps. All rights reserved.
 */

package uk.co.libertyapps.covid.app.onboarding

sealed class PostCodeNavigation {
    object Permissions : PostCodeNavigation()
}
