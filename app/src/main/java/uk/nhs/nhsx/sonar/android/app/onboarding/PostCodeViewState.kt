/*
 * Copyright © 2020 Liberty Apps. All rights reserved.
 */

package uk.co.libertyapps.covid.app.onboarding

sealed class PostCodeViewState {
    object Valid : PostCodeViewState()
    object Invalid : PostCodeViewState()
}
