/*
 * Copyright © 2020 Liberty Apps. All rights reserved.
 */

package uk.co.libertyapps.covid.app.registration

sealed class RegistrationResult {
    object Success : RegistrationResult()
    object WaitingForActivationCode : RegistrationResult()
    object Error : RegistrationResult()
}
