/*
 * Copyright © 2020 Liberty Apps. All rights reserved.
 */

package uk.co.libertyapps.covid.app.status

import android.app.Activity
import uk.co.libertyapps.covid.app.status.DisplayState.AT_RISK
import uk.co.libertyapps.covid.app.status.DisplayState.ISOLATE
import uk.co.libertyapps.covid.app.status.DisplayState.OK

fun Activity.navigateTo(state: UserState) {
    when (state.displayState()) {
        OK -> {
            if (this is OkActivity) return

            OkActivity.start(this)
            finish()
            overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out)
        }
        AT_RISK -> {
            if (this is AtRiskActivity) return

            AtRiskActivity.start(this)
            finish()
            overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out)
        }
        ISOLATE -> {
            if (this is IsolateActivity) return

            IsolateActivity.start(this)
            finish()
            overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out)
        }
    }
}
