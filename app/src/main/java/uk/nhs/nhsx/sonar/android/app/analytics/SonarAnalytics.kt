/*
 * Copyright © 2020 Liberty Apps. All rights reserved.
 */

package uk.co.libertyapps.covid.app.analytics

interface SonarAnalytics {
    fun trackEvent(event: AnalyticEvent)
}
