/*
 * Copyright © 2020 Liberty Apps. All rights reserved.
 */

package uk.co.libertyapps.covid.app.di.module

import android.bluetooth.BluetoothAdapter
import android.bluetooth.BluetoothManager
import android.bluetooth.le.BluetoothLeAdvertiser
import android.content.Context
import androidx.core.content.ContextCompat.getSystemService
import com.polidea.rxandroidble2.RxBleClient
import dagger.Module
import dagger.Provides
import uk.co.libertyapps.covid.app.DeviceDetection
import uk.co.libertyapps.covid.app.ble.BleEvents
import uk.co.libertyapps.covid.app.ble.SaveContactWorker
import uk.co.libertyapps.covid.app.ble.Scanner
import javax.inject.Named

@Module
open class BluetoothModule(
    private val applicationContext: Context,
    private val scanIntervalLength: Int
) {
    @Provides
    fun provideBluetoothManager(): BluetoothManager =
        getSystemService(applicationContext, BluetoothManager::class.java)!!

    @Provides
    fun provideBluetoothAdvertiser(bluetoothManager: BluetoothManager): BluetoothLeAdvertiser =
        bluetoothManager.adapter.bluetoothLeAdvertiser

    @Provides
    open fun provideRxBleClient(): RxBleClient =
        RxBleClient.create(applicationContext)

    @Provides
    open fun provideDeviceDetection(): DeviceDetection =
        DeviceDetection(BluetoothAdapter.getDefaultAdapter(), applicationContext)

    @Provides
    open fun provideScanner(
        rxBleClient: RxBleClient,
        saveContactWorker: SaveContactWorker,
        bleEvents: BleEvents
    ): Scanner =
        Scanner(
            rxBleClient,
            saveContactWorker,
            bleEvents,
            scanIntervalLength = scanIntervalLength
        )

    @Provides
    @Named(SCAN_INTERVAL_LENGTH)
    fun provideScanIntervalLength() = scanIntervalLength

    companion object {
        const val SCAN_INTERVAL_LENGTH = "SCAN_INTERVAL_LENGTH"
    }
}
