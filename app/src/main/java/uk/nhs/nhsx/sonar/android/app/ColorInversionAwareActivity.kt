/*
 * Copyright © 2020 Liberty Apps. All rights reserved.
 */

package uk.co.libertyapps.covid.app

import androidx.annotation.LayoutRes
import androidx.appcompat.app.AppCompatActivity
import uk.co.libertyapps.covid.app.util.isInversionModeEnabled

abstract class ColorInversionAwareActivity : AppCompatActivity {

    constructor() : super()

    constructor(@LayoutRes layoutId: Int) : super(layoutId)

    override fun onResume() {
        super.onResume()
        handleInversion(isInversionModeEnabled())
    }

    open fun handleInversion(inversionModeEnabled: Boolean) {
    }
}
