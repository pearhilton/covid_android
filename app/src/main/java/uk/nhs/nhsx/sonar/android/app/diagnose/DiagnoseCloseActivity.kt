/*
 * Copyright © 2020 Liberty Apps. All rights reserved.
 */

package uk.co.libertyapps.covid.app.diagnose

import android.content.Context
import android.content.Intent
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_review_close.close_review_btn
import kotlinx.android.synthetic.main.activity_review_close.nhs_service
import kotlinx.android.synthetic.main.symptom_banner.toolbar
import uk.co.libertyapps.covid.app.BaseActivity
import uk.co.libertyapps.covid.app.R
import uk.co.libertyapps.covid.app.appComponent
import uk.co.libertyapps.covid.app.status.UserStateStorage
import uk.co.libertyapps.covid.app.status.navigateTo
import uk.co.libertyapps.covid.app.util.URL_SYMPTOM_CHECKER
import uk.co.libertyapps.covid.app.util.openUrl
import javax.inject.Inject

class DiagnoseCloseActivity : BaseActivity() {
    @Inject
    protected lateinit var userStateStorage: UserStateStorage

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        appComponent.inject(this)

        setContentView(R.layout.activity_review_close)

        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setHomeAsUpIndicator(R.drawable.ic_arrow_back)
        toolbar.setNavigationOnClickListener { onBackPressed() }

        close_review_btn.setOnClickListener {
            navigateTo(userStateStorage.get())
        }

        nhs_service.setOnClickListener {
            openUrl(URL_SYMPTOM_CHECKER)
        }
    }

    override fun handleInversion(inversionModeEnabled: Boolean) {
        if (inversionModeEnabled) {
            close_review_btn.setBackgroundResource(R.drawable.button_round_background_inversed)
        } else {
            close_review_btn.setBackgroundResource(R.drawable.button_round_background)
        }
    }

    companion object {
        fun start(context: Context) =
            context.startActivity(getIntent(context))

        private fun getIntent(context: Context) =
            Intent(context, DiagnoseCloseActivity::class.java)
    }
}
