/*
 * Copyright © 2020 Liberty Apps. All rights reserved.
 */

package uk.co.libertyapps.covid.app.edgecases

import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import uk.co.libertyapps.covid.app.R

class DeviceNotSupportedActivity : AppCompatActivity(R.layout.activity_device_not_supported) {

    companion object {
        fun start(context: Context) = context.startActivity(getIntent(context))

        fun getIntent(context: Context) = Intent(context, DeviceNotSupportedActivity::class.java)
    }
}
