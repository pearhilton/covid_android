/*
 * Copyright © 2020 Liberty Apps. All rights reserved.
 */

package uk.co.libertyapps.covid.app.notifications

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query

@Dao
interface AcknowledgmentsDao {

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insert(acknowledgment: Acknowledgment)

    @Query("SELECT * FROM ${Acknowledgment.TABLE_NAME} WHERE url = :url")
    fun tryFind(url: String): Acknowledgment?
}
